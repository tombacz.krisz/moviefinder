// Use Express
const express = require("express");
const bodyParser = require("body-parser");
const axios = require('axios');
const app = express();
const distDir = __dirname + "/dist/";
const API_KEY = 'bc2db20072844e2b9baaee14a6854d7c';
const DOMAIN = 'https://api.themoviedb.org/3'
app.use(bodyParser.json());
app.use(express.static(distDir));

// Init the server
const server = app.listen(process.env.PORT || 8080, function () {
    const port = server.address().port;
    console.log("App now running on port", port);
});

app.get("/api/status", (req, res) => {
    res.status(200).json({ status: "UP" });
});

app.get("/api/getGenres", (req, res) => {
    axios.get(`${DOMAIN}/genre/movie/list?api_key=${API_KEY}&language=en-US`)
    .then(response => {
        res.status(200).json({ data: response.data })})
    .catch(error => {
        console.log(error);
    });
});

app.get("/api/getMovie/:id", (req, res) => {
    const id = req.params['id'];
    axios.get(`${DOMAIN}/movie/${id}?api_key=${API_KEY}&language=en-US`)
    .then(response => {
        res.status(200).json({ data: response.data })})
    .catch(error => {
        console.log(error);
    });
});

app.get("/api/getSimilarMovies/:id", (req, res) => {
    const id = req.params['id'];
    axios.get(`${DOMAIN}/movie/${id}/similar?api_key=${API_KEY}&language=en-US`)
    .then(response => {
        res.status(200).json({ data: response.data })})
    .catch(error => {
        console.log(error);
    });
});

app.get("/api/wiki/:title", (req, res) => {
    const title = req.params['title'];
    axios.get(`https://en.wikipedia.org/w/api.php?format=json&action=query&titles=${title}&prop=revisions&rvprop=content`)
    .then(response => {
        res.status(200).json({ data: response.data })})
    .catch(error => {
        console.log(error);
    });
});

app.post("/api/getMovies", (req, res) => {
    const query = req.body.searchText;
    const page = req.body.page;
    axios.get(`${DOMAIN}/search/movie?api_key=${API_KEY}&language=en-US&query=${query}&page=${page}&include_adult=false`)
    .then(response => {
        res.status(200).json({ data: response.data })})
    .catch(error => {
        console.log(error);
    });
});