import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { SearchBannerComponent } from './components/search-banner/search-banner.component';
import { MovieListComponent } from './components/movie-list/movie-list.component';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import { DetailPageComponent } from './pages/detail-page/detail-page.component';
import { CommonModule } from '@angular/common';
import { FormatTextPipe } from './shared/format-text.pipe';

const routes: Routes = [
  { path: '',   redirectTo: '/moviefinder', pathMatch: 'full' },
  { path: 'moviefinder/:movieId', component: DetailPageComponent},
  { path: 'moviefinder', component: SearchPageComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    SearchPageComponent,
    SearchBannerComponent,
    MovieListComponent,
    DetailPageComponent,
    FormatTextPipe
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    NoopAnimationsModule,
    RouterModule.forRoot(routes),
    MatInputModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule { }
