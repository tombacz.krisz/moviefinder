import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movie-detail-card',
  templateUrl: './movie-detail-card.component.html',
  styleUrls: ['./movie-detail-card.component.css']
})
export class MovieDetailCardComponent {}
