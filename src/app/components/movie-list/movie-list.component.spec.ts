import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MovieDetail } from 'src/app/shared/movie-data';

import { MovieListComponent } from './movie-list.component';

describe('MovieListComponent', () => {
  let component: MovieListComponent;
  let fixture: ComponentFixture<MovieListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovieListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create table', () => {
    component.genreList = [{id: 1, name: 'test'}];
    component.movieList = {
      page: 1,
      results: [{id: 2, title: 'test', genre_ids: [1], vote_average: 3} as MovieDetail],
      total_pages: 1,
      total_results: 2
    };
    component.ngOnChanges({movieList: {currentValue: 1}} as any)
    expect(component.tableData).toEqual([{id: 2, title: 'test', genres: 'test', voteAverage: 3}]);
    expect(component.totalResults).toEqual(2);
  });
});
