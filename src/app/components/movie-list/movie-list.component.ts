import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MovieData } from 'src/app/shared/movie-data';
import { MovieGenre } from 'src/app/shared/movie-genre';

interface MovieTableData {
  id: number,
  title: string,
  genres: string,
  voteAverage: number,
}


@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnChanges {

  public tableData: MovieTableData[];
  public totalResults = 0;
  public displayedColumns = ['title', 'genres', 'voteAverage', 'similar'];

  @Input() movieList: MovieData = undefined;
  @Input() genreList: MovieGenre[] = [];
  @Output() similarMoviesLoad = new EventEmitter<number>();
  @Output() paginatorClick = new EventEmitter<any>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.movieList?.currentValue) {
      this.createTableData();
    }
  }

  private createTableData(): void {
    this.totalResults = this.movieList.total_results;
    this.tableData = this.movieList.results.map(movie => ({
      id: movie.id,
      title: movie.title,
      genres: this.addressGenres(movie.genre_ids),
      voteAverage: movie.vote_average,
    }));
  }

  private addressGenres(idList: number[]): string {
    return idList.map(id => 
      this.genreList.find(genre => genre.id === id)?.name
    ).join(', ');
  }
}
