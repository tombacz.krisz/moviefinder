import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieReleatedListComponent } from './movie-releated-list.component';

describe('MovieReleatedListComponent', () => {
  let component: MovieReleatedListComponent;
  let fixture: ComponentFixture<MovieReleatedListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovieReleatedListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieReleatedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
