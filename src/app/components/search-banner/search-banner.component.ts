import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-search-banner',
  templateUrl: './search-banner.component.html',
  styleUrls: ['./search-banner.component.css']
})
export class SearchBannerComponent implements OnChanges {
  public searchValue = '';

  @Input() loadedValue: string;
  @Output() search = new EventEmitter<string>();

  ngOnChanges(changes: SimpleChanges): void {
    if (!!changes.loadedValue?.currentValue) {
      this.searchValue = this.loadedValue;
    }
  }
}
