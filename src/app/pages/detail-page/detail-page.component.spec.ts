import { NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { ConnectionService } from 'src/app/shared/connection.service';
import { DetailPageComponent } from './detail-page.component';

const wikiResponse = new BehaviorSubject<any>({wikiData: {data: {query: {pages: [-1]}}}});

class MockConnectionService {
  loadMovie(arg1: string) {
    return of({imdb_id: 1, original_title: 'test', homepage: 'test'});
  }

  loadWikiDetails(arg2: string) {
    return wikiResponse;
  }
}

describe('DetailPageComponent', () => {
  let component: DetailPageComponent;
  let fixture: ComponentFixture<DetailPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailPageComponent, MockPipe],
      providers: [
        { provide: ConnectionService, useClass: MockConnectionService },
        { provide: ActivatedRoute, useValue: {
            params: { pipe: () => of({movieId: 1}) }
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Pipe({name: 'formatText'})
class MockPipe implements PipeTransform {
  transform(value: string) {
    return 'test';
  }
}
