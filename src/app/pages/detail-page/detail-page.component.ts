import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { ConnectionService } from 'src/app/shared/connection.service';
import { MovieFullData } from 'src/app/shared/movie-data';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.css']
})
export class DetailPageComponent implements OnInit {

  public movieDetails$ = new Observable<string>();
  public imdbId = '';
  public wikiTitle = '';
  public hompage = '';

  constructor(
    private readonly connectionService: ConnectionService,
    private readonly route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadMovieDetails();
  }

  private loadMovieDetails(): void {
    this.movieDetails$ = this.route.params.pipe(
      take(1),
      switchMap(param => this.connectionService.loadMovie(param['movieId'])),
      tap((movie: MovieFullData) => {
        this.imdbId = movie.imdb_id;
        this.wikiTitle = movie.original_title;
        this.hompage = movie.homepage;
      }),
      switchMap(movie => this.connectionService.loadWikiDetails(movie.original_title)),
      map(wikiData => {
        const page = wikiData.data?.query?.pages
        
        if(Object.keys(page)[0] != '-1') {
          return (Object.values(page)[0] as any)?.revisions[0]['*'] ?? ''
        }

        return 'No relevant data has been found on wiki pages!';
      })
    )
  }

}
