import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { ConnectionService } from 'src/app/shared/connection.service';

import { SearchPageComponent } from './search-page.component';

class MockConnectionService {
  loadGenres() {
    return of([]);
  }

  loadMovies(arg1: string, arg2: number) {
    return arg2 === 1 ? of({total_results: 1}) : of({total_results: 3});
  }

  loadSimilarMovies(arg2: number) {
    return of({total_results: 2});
  }
}

describe('SearchPageComponent', () => {
  let component: SearchPageComponent;
  let fixture: ComponentFixture<SearchPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchPageComponent ],
      providers: [
        { provide: ConnectionService, useClass: MockConnectionService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should give back movie data if search text is given', done => {
    component.movieList$.subscribe((data: any) => {
      expect(data).toEqual({total_results: 1});
      done();
    });
    component.onSearch('test');
  });

  it('should load back stored value', () => {
    spyOn(window.sessionStorage, 'getItem').and.returnValue('test');
    expect(component.loadedText).toEqual('test');
  });

  it('should give back movie data if similar search is called', done => {
    component.movieList$.subscribe((data: any) => {
      expect(data).toEqual({total_results: 2});
      done();
    });
    component.loadSimilarMovies(2);
  });

  it('should give back movie data if next page is called', done => {
    component.movieList$.subscribe((data: any) => {
      expect(data).toEqual({total_results: 3});
      done();
    });
    component.loadPage({pageIndex: 1});
  });

});
