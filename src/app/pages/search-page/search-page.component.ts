import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';
import { ConnectionService } from 'src/app/shared/connection.service';
import { MovieData } from 'src/app/shared/movie-data';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit, OnDestroy {

  private search$ = new Subject<string>();
  public genreList$ = this.connectionService.loadGenres();
  public movieList$ = new Subject<MovieData>();
  public loadedText = '';

  constructor(private readonly connectionService: ConnectionService) { }

  ngOnInit(): void {
    this.search$.pipe(
      switchMap(searchText => this.connectionService.loadMovies(searchText, 1)),
      tap((movies:MovieData) => this.movieList$.next(movies))
    ).subscribe();

    const searchText = sessionStorage.getItem('searchText');
    if (!!searchText) {
      this.genreList$.pipe(
        take(1),
        tap(() => this.search$.next(searchText)),
        tap(() => this.loadedText = searchText)
      ).subscribe();
    }
  }

  ngOnDestroy(): void {
    if (this.search$) {
      this.search$.complete();
    }
    if (this.movieList$) {
      this.movieList$.complete();
    }
  }

  public onSearch(searchText: string): void {
    this.search$.next(searchText);
    sessionStorage.setItem('searchText', searchText);
  }

  public loadSimilarMovies(id: number): void {
    this.connectionService.loadSimilarMovies(id).pipe(
      tap((movies:MovieData) => this.movieList$.next(movies))
    ).subscribe();
  }

  public loadPage(event: any): void {
    this.connectionService.loadMovies(this.loadedText, event.pageIndex + 1).pipe(
      tap((movies:MovieData) => this.movieList$.next(movies))
    ).subscribe();
  }
}
