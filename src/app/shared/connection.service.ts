import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MovieGenre, MovieGenreResponse } from './movie-genre';
import { MovieData, MovieDataResponse } from './movie-data';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  constructor(private http: HttpClient) { }

  loadGenres(): Observable<MovieGenre[]> {
    return this.http.get('api/getGenres').pipe(
      map((resp: MovieGenreResponse) => 
        resp?.data ? resp.data.genres : [])
    )
  }

  loadMovie(id: number): Observable<any> {
    return this.http.get(`/api/getMovie/${id}`).pipe(
      map((resp: any) => resp.data)
    );
  }

  loadMovies(searchText: string, page: number): Observable<MovieData> {
    return this.http.post('api/getMovies', {searchText, page}).pipe(
      map((resp: MovieDataResponse) => resp.data)
    );
  }

  loadSimilarMovies(id: number): Observable<any> {
    return this.http.get(`/api/getSimilarMovies/${id}`).pipe(
      map((resp: any) => resp.data)
    );
  }

  loadWikiDetails(movieTitle: string): Observable<any> {
    movieTitle = movieTitle.replace(/ /gm, '%20');
    return this.http.get(`api/wiki/${movieTitle}`);
  }
}
