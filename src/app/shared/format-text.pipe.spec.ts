import { FormatTextPipe } from './format-text.pipe';

describe('FormatTextPipe', () => {

  it('create an instance', () => {
    const pipe = new FormatTextPipe();
    expect(pipe).toBeTruthy();
  });

  it('sould give back empty string if the input string is empty', () => {
    const pipe = new FormatTextPipe();
    const value = pipe.transform('');
    expect(value).toEqual('')
  });

  it('sould give back string with proper transformation', () => {
    const pipe = new FormatTextPipe();
    const value = pipe.transform('{[id|=test');
    expect(value).toEqual('id |: test');
  });
});
