import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatText'
})
export class FormatTextPipe implements PipeTransform {

  transform(value: string): string {
    if (!value) {
      return '';
    }
    let formattedText = value.replace(/{|}/gm, '');
    formattedText = formattedText.replace(/\|/gm, ' | ');
    formattedText = formattedText.replace(/=| =/gm, ': ');
    formattedText = formattedText.replace(/\[|\]]/gm, '');
    return formattedText;
  }

}
