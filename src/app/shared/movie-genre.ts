export interface MovieGenre {
    id: number,
    name: string,
}

export interface MovieGenreResponse {
    data: {
        genres: MovieGenre[],
    }
}